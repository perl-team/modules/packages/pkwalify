pkwalify (1.24-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.24.
  * Update years of upstream copyright.
  * Declare compliance with Debian Policy 4.7.0.

 -- gregor herrmann <gregoa@debian.org>  Sun, 28 Jul 2024 16:38:11 +0200

pkwalify (1.23-3) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Update standards version to 4.6.1, no changes needed.

  [ gregor herrmann ]
  * Set Rules-Requires-Root: no.
  * Update alternative test dependencies.
  * Annotate test-only build dependencies with <!nocheck>.
  * Add patch to fix autopkgtest's smoke test.
    Thanks to Paul Gevers for the bug report. (Closes: #1023709)
  * Enable autopkgtest's use.t test.

 -- gregor herrmann <gregoa@debian.org>  Fri, 11 Nov 2022 15:57:00 +0100

pkwalify (1.23-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from deprecated 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Submit, Repository-Browse.
  * Apply multi-arch hints. + pkwalify: Add Multi-Arch: foreign.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.
  * Set Testsuite header for perl package.
  * Remove constraints unnecessary since buster

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 14 Oct 2022 14:30:05 +0100

pkwalify (1.23-1) unstable; urgency=medium

  [ Axel Beckert ]
  * Make debian/rules executable.

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Victor Seva ]
  * New upstream version 1.23
  * use YAML::PP instead of YAML::Syck for tests. wrap-and-sort -sat
    (Closes: #952158)
  * fix yaml format of debian/upstream/metadata
  * update Standards-Version, no changes needed
  * annotate perl dependency as Multiarch hinter points

 -- Victor Seva <vseva@debian.org>  Mon, 24 Feb 2020 10:41:34 +0100

pkwalify (1.22.99~git3d3f0ea-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: use HTTPS for GitHub URLs.

  [ Victor Seva ]
  * [c2d7ff6] New upstream version 1.22.99~git3d3f0ea
  * [7d4a3d4] debian/control: use debhelper >= 9
  * [7ff344d] debian/control: Standards-Version 4.0.0 no changes needed
  * [47c94a7] debian/control: use my DD account
  * [5125248] debian/patches: remove fix_manpages.patch

 -- Victor Seva <vseva@debian.org>  Mon, 10 Jul 2017 10:02:33 +0200

pkwalify (1.22-2) unstable; urgency=medium

  * [c8abe5a] clean fix_manpages.patch
  * [17c5154] fix copyright perl is GPL-1+ or Artistic

 -- Victor Seva <linuxmaniac@torreviejawireless.org>  Mon, 31 Aug 2015 17:44:31 +0200

pkwalify (1.22-1) unstable; urgency=low

  * Initial Release (closes: #792031).

 -- Victor Seva <linuxmaniac@torreviejawireless.org>  Fri, 10 Jul 2015 11:41:05 +0200
